from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.urls import reverse


# Create your models here.
class Topic(models.Model):
	topic_name = models.CharField(max_length = 100)
	def __str__(self):
		return self.topic_name

class Question(models.Model):
	subject = models.ForeignKey(Topic, on_delete=models.CASCADE)
	question = models.CharField(max_length = 200)
	ans = models.IntegerField()
	choice_1 = models.CharField(max_length = 100)
	choice_2 = models.CharField(max_length = 100)
	choice_3 = models.CharField(max_length = 100)
	choice_4 = models.CharField(max_length = 100)

	grade = models.IntegerField(default = 5)

	def __str__(self):
		return self.question

class Post(models.Model):
	title = models.CharField( max_length = 100 )
	content = models.TextField()
#date_posted = models.DateTimeField( auto_now_add = True ) this gets the current time and date that it is created.
	date_posted = models.DateTimeField( default = timezone.now )
	author = models.ForeignKey(User, on_delete = models.CASCADE) #if a user is deleted then their post goes too

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('forum-detail', kwargs={'pk': self.pk})

class LeaderBoard(models.Model):
	topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	marks = models.IntegerField()
	names = models.IntegerField(default = 9)


# Create your models here.





	