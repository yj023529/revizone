from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from . import views
from .views import PostListView, PostDetailView,  PostCreateView, PostUpdateView, PostDeleteView


urlpatterns = [
  
    path('forum/', PostListView.as_view(), name = 'elearning-forum'),
    path('post/<int:pk>/', PostDetailView.as_view(), name = 'forum-detail'),
    path('forum/new/', PostCreateView.as_view(), name='forum-create'),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='forum-update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='forum-delete'),

    path('uploadFile/', views.uploadfile_view, name = 'elearning-uploadfile'),
    path('', views.home, name = 'elearning-home'),
    path('count1/', views.count1, name = 'elearning-count1'),
    path('count2/', views.count2, name = 'elearning-count2'),
    path('count3/', views.count3, name = 'elearning-count3'),
    path('count4/', views.count4, name = 'elearning-count4'),
    path('count5/', views.count5, name = 'elearning-count5'),
    path('count6/', views.count6, name = 'elearning-count6'),
    path('count7/', views.count7, name = 'elearning-count7'),
    path('count8/', views.count7, name = 'elearning-count8'),
    path('preferences/', views.preferences, name = 'elearning-preferences'),

    path('about/', views.about, name = 'elearning-about'),
    path('exam/', views.exam, name = 'elearning-Biological_Molecules_exam'),
    path('notes/', views.notes, name = 'elearning-notes'),
    path('Leaderboard/', views.Leaderboard, name = 'elearning-Leaderboard'),
    path('cells/', views.cells, name = 'elearning-cells'),
    path('energy/', views.energy, name = 'elearning-energy'),
    path('genes/', views.genes, name = 'elearning-genes'),
    path('Genetics/', views.Genetics, name = 'elearning-Genetics'),
    path('Organisms/', views.Organisms, name = 'elearning-Organisms'),
    path('organisms_reaction/', views.organisms_reaction, name = 'elearning-organisms_reaction'),
    path('populations/', views.populations, name = 'elearning-populations'),
    path('Biological_molecules/', views.Biological_molecules, name = 'elearning-Biological_molecules'),

    path('BioMoleculesQuiz/', views.BioMoleculesQuiz, name = 'elearning-BioMoleculesQuiz'),
    path('Biological_Molecules_notes1/', views.Biological_Molecules_notes1, name = 'elearning-Biological_Molecules_notes1'),
    path('Biological_Molecules_notes2/', views.Biological_Molecules_notes2, name = 'elearning-Biological_Molecules_notes2'),
    path('Biological_Molecules_notes3/', views.Biological_Molecules_notes3, name = 'elearning-Biological_Molecules_notes3'),
    path('Biological_Molecules_notes4/', views.Biological_Molecules_notes4, name = 'elearning-Biological_Molecules_notes4'),
    path('Biological_Molecules_notes5/', views.Biological_Molecules_notes5, name = 'elearning-Biological_Molecules_notes5'),
    path('quizes/', views.quizes, name = 'elearning-quizes'),
    
    path('quiz/', views.quiz, name = 'elearning-Quiz'),
    path('<id>' , views.start_test, name = "start_test"),
    path('mcq/<id>', views.mcq_question, name = "mcq_question"),

    path('view_marks/', views.view_marks, name = "view_marks"),
    path('check_marks', views.check_marks, name = "check_marks" ),

    ]

if settings.DEBUG:
    urlpatterns + static(settings.STATIC_URL,document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,document_root= settings.MEDIA_ROOT)