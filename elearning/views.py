import django
import json
import random
from django.shortcuts import render
from .models import Post, Topic, Question, LeaderBoard, User
from users.models import Profile
# from .forms import CountForm
from django.http import FileResponse, Http404
from django.http import JsonResponse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
# from django.core.context_processors import csrf
from django.core.files.storage import FileSystemStorage# from exam.models import Question, Choice
from django.views.generic import ListView
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


# def test(request):
#     questions = Question.objects.all()
#     answers = Choice.objects.all()

#     return render(request, 'index.html', {"Question":questions, "Choice":answers})

def preferences (request):
    # user = request.user
    # current_user = Profile.objects.get (user = user)
    # current_user.notesprogress += 13
    # current_user.save ()
    # notesprogress = Profile.objects.get (user = user) .notesprogress
    user = request.user
    current_user = Profile.objects.get (user = user)
    quiz_preference = Profile.objects.get (user = user) .quiz_preference
    if current_user.quiz_preference == True:
        current_user.quiz_preference = False
    else:
        current_user.quiz_preference = True

    current_user.save ()
    return render (request, 'elearning/home.html', {'preference': quiz_preference})



def forum (request):
    context = {
        'posts' : Post.objects.all()
    }
    return render(request,'forum.html', context)
    

class PostListView(ListView):
    model = Post
    template_name = 'forum.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']

class PostDetailView(DetailView):
    model = Post 


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/forum/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False




def count1 (request):
    # user = request.user
    # current_user = Profile.objects.get (user = user)
    # current_user.notesprogress += 13
    # current_user.save ()
    # notesprogress = Profile.objects.get (user = user) .notesprogress
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress
    if current_user.molecules_progress ==20:
        current_user.molecules_progress = 100
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count1': notesprogress})

def count2 (request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress

    if current_user.cells_progress ==20:
        current_user.cells_progress = 100
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count2': notesprogress})

def count3 (request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress

    if current_user.organ_exchange_progress ==20:
        current_user.organ_exchange_progress = 100
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count3': notesprogress})

def count4 (request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress

    if current_user.genes_info_progress ==20:
        current_user.genes_info_progress = 100
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count4': notesprogress})

def count5 (request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress

    if current_user.energy_transfer ==20:
        current_user.energy_transfer = 100
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count5': notesprogress})

def count6 (request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress

    if current_user.organ_response_progress ==20:
        current_user.organ_response_progress = 100
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count6': notesprogress})

def count7 (request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress

    if current_user.genes_pop_progress ==20:
        current_user.genes_pop_progress = 100
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count7': notesprogress})    

def count8 (request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    notesprogress = Profile.objects.get (user = user) .notesprogress

    if current_user.gene_control_progress ==20:
        current_user.gene_control_progress += 80
        current_user.notesprogress += 10

    current_user.save ()
    return render (request, 'elearning/notes.html', {'count8': notesprogress})    

def home(request):
    
    return render(request, 'elearning/home.html')


def about(request):

    return render(request, 'elearning/about.html', {'title': 'About'})

def notes(request):
  
    return render(request, 'elearning/notes.html', {'title': 'Notes'}) 

def cells(request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.cells_progress ==0:
        current_user.cells_progress += 20
        current_user.notesprogress += 2.5
    current_user.save ()
    return render(request, 'elearning/topics/cells/cells.html', {'title': 'Cells'})     

def energy(request):
    
    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.energy_transfer == 0:
        current_user.energy_transfer += 20
        current_user.notesprogress += 2.5
    current_user.save ()

    return render(request, 'elearning/topics/energy/energy.html', {'title': 'Energy Transfers'}) 

def genes(request):

    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.gene_control_progress == 0:
        current_user.gene_control_progress += 20
        current_user.notesprogress += 2.5

    current_user.save ()
    
    return render(request, 'elearning/topics/genes/genes.html', {'title': 'The control of gene expression'}) 

def Genetics(request):

    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.genes_info_progress == 0:
        current_user.genes_info_progress += 20
        current_user.notesprogress += 2.5
    current_user.save ()
    
    return render(request, 'elearning/topics/Genetics/Genetics.html', {'title': 'Genetic information'})  


def Biological_molecules(request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.molecules_progress == 0:
        current_user.molecules_progress += 20
        current_user.notesprogress += 2.5
    current_user.save ()
   
    return render(request, 'elearning/topics/Biological_molecules/Biological_molecules.html', {'title': 'Biological_molecules'}) 

def Biological_Molecules_notes1(request):
    
    return render(request, 'elearning/topics/Biological_molecules/Biological_Molecules_notes1.html', {'title': 'Biological_Molecules_notes1'}) 

def Biological_Molecules_notes2(request):
    
    return render(request, 'elearning/topics/Biological_molecules/Biological_Molecules_notes2.html', {'title': 'Biological_Molecules_notes2'}) 
def Biological_Molecules_notes3(request):
    
    return render(request, 'elearning/topics/Biological_molecules/Biological_Molecules_notes3.html', {'title': 'Biological_Molecules_notes3'}) 
def Biological_Molecules_notes4(request):
    
    return render(request, 'elearning/topics/Biological_molecules/Biological_Molecules_notes4.html', {'title': 'Biological_Molecules_notes4'}) 
def Biological_Molecules_notes5(request):
    
    return render(request, 'elearning/topics/Biological_molecules/Biological_Molecules_notes5.html', {'title': 'Biological_Molecules_notes5'}) 


def Organisms(request):

    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.organ_exchange_progress == 0:
        current_user.organ_exchange_progress += 20
        current_user.notesprogress += 2.5
    current_user.save ()

    return render(request, 'elearning/topics/Organisms/Organisms.html', {'title': 'Organisms exchange substances'})     
                 
def organisms_reaction(request):
    
    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.organ_response_progress == 0:
        current_user.organ_response_progress += 20
        current_user.notesprogress += 2.5
    current_user.save ()

    return render(request, 'elearning/topics/organisms_reaction/organisms_reaction.html', {'title': 'Organisms Reaction'}) 

def populations(request):
    user = request.user
    current_user = Profile.objects.get (user = user)
    if current_user.genes_pop_progress == 0:
        current_user.genes_pop_progress += 20
        current_user.notesprogress += 2.5
    current_user.save ()

    return render(request, 'elearning/topics/populations/populations.html', {'title': 'Genetics, Populations, Evolution'})

def exam(request):
    
    return render(request, 'elearning/topics/Biological_molecules/exam.html', {'title': 'exam'})

def quizes(request):
    
    return render(request, 'elearning/quizes.html', {'title': 'quizes'})

def BioMoleculesQuiz(request):
    
    return render(request, 'elearning/QuizTopics/BioMoleculesQuiz.html/', {'title': 'BioMoleculesQuiz'}) 


def uploadfile_view(request):
    if request.method == 'POST':
        f = request.FILES['file']
        fs = FileSystemStorage()
        filename,ext = str(f).split('.')
        file = fs.save(str(f),f)
        fileurl= fs.url(file)
        size = fs.size(file)
        return render(request, 'elearning/topics/Biological_molecules/Biological_Molecules_notes1.html', {'fileUrl':fileurl, "fileName" : filename, "ext":ext, "size":size})

    else:
        return render(request, 'elearning/topics/Biological_molecules/Biological_Molecules_notes1.html',{} )

def Leaderboard(request):
    user = request.user
    marks = LeaderBoard.objects.filter(user = user)
    allmarks = LeaderBoard.objects.all()

    context = {'marks' : marks}
    return render(request, 'elearning/Leaderboard.html', context)



def quiz(request):
    topics = Topic.objects.all()
    context = {'topics' : topics}
    return render(request, 'elearning/QuizTopics/quiz.html/', context)

def mcq_question(request, id):
    
    spliter = random.randint(0,4)
    on_questions = Question.objects.filter(subject = id)[spliter:spliter+15]
    questions = []
    counter = 1


    for on_question in on_questions:
        question = {}
        question['question'] = on_question.question
        question['ans'] = on_question.ans
        question['grade'] = on_question.grade
        counter += 1

        question['id'] = counter
        choices = []

        choices.append(on_question.choice_1)
        choices.append(on_question.choice_2)
        choices.append(on_question.choice_3)
        choices.append(on_question.choice_4)

        question['choices'] = choices

        questions.append(question)

    return JsonResponse(questions , safe = False)




def start_test(request, id):
    context = {'id' : id}
    return render(request, 'elearning/QuizTopics/exam.html/', context)


@csrf_exempt
def view_marks(request):
   
    user = request.user
    marks = LeaderBoard.objects.filter(user=user)
    context = { 'marks' : marks }
    
    return render(request, 'elearning/QuizTopics/score.html', context)
    # info = json.loads(request.body)
    # user = request.user
    # course_id = info.get('course_id')
    # solutions = json.loads(info.get('info'))
    # marks= 0
    # for solution in solutions:
    #     question = Question.objects.filter(id = solution.get('question_id')).first()
    #     print(solution.get('choices'))
    #     if str(question.ans) == solution.get('choices'):
    #         marks = marks + question.grade

    # leaderboard = LeaderBoard(topic = course, marks = marks, user = user)
    # leaderboard.save()

    # course = Topic.objects.get(id = course_id)
    # print(course)
    # print(course_id)
    # print(solutions)
    # return JsonResponse({'message' : 'success', 'status' : True})

    
@csrf_exempt
def check_marks(request):
    c = {}
    c.update(csrf(request))
    data = json.loads(request.body)
    user = request.user
    course_id = data.get('course_id')
    answers = json.loads(data.get('data'))
    course = Course.objects.get(id = course_id)
    mark = 0

    for answer in answers:
        question = Question.objects.filter(id = solution.get('question_id')).first()
        print(solution.get(Choice))
        if str(question.answer) ==solution.get(Choice):
            mark = mark + question.grade

    leaderboard = LeaderBoard(course = course, mark = mark, user = user)
    leaderboard.save()

    course = Topic.objects.get(id = course_id)
    print(course)
    print(course_id)
    print(solutions)
    return JsonResponse({'message' : 'success' , 'status' : True}, c)

# def database(request, id):
    
#     spliter = random.randint(0,4)
#     on_questions = Question.objects.filter(subject = id)[spliter:spliter+15]
#     questions = []
#     counter = 1


#     for on_question in on_questions:
#         question = {}
#         question['question'] = on_question.question
#         question['ans'] = on_question.ans
#         question['grade'] = on_question.grade
#         counter += 1

#         question['id'] = counter
#         choices = []

#         choices.append(on_question.choice_1)
#         choices.append(on_question.choice_2)
#         choices.append(on_question.choice_3)
#         choices.append(on_question.choice_4)

#         question['choices'] = choices

#         questions.append(question)

#     return JsonResponse(questions , safe = False)






