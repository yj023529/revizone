from django.contrib import admin
from.models import Post
from.models import Topic
from.models import Question, LeaderBoard
# Register your models here.


admin.site.site_header = 'Revizone Admin Dashboard'
admin.site.register(Post)
admin.site.register(Topic)
admin.site.register(Question)
admin.site.register(LeaderBoard)


