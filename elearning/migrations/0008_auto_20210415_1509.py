# Generated by Django 3.1.3 on 2021-04-15 15:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('elearning', '0007_auto_20210218_1545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='question',
            field=models.CharField(max_length=200),
        ),
    ]
