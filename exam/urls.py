from django.urls import path
from .views import QuestionListView
from . import views

urlpatterns = [
	path('BioQuiz', views.test, name = 'exam-home'),
	path('about/', views.about, name = 'exam-about'),
]