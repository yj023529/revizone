
from django.shortcuts import render
from exam.models import Question, Choice


def test(request):
	questions = Question.objects.all()
	answers = Choice.objects.all()

	return render(request, 'index.html', {"Question":questions, "Choice":answers})

def home(request):
    context = {
        'Question': Question.objects.all()
    }
    return render(request, 'exam/index.html', context)

class QuestionListView(ListView)
	model = Question


def about(request):
    return render(request, 'elearning/about.html', {'title': 'About'})