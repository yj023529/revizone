from django.db import models
from django.contrib.auth.models import User
 










class Choice(models.Model):
    choice = models.CharField(max_length=200)
    is_answer = models.BooleanField(default=False)
    def __str__(self):
        return self.choice
 
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    choice = models.ManyToManyField(Choice)
    def __str__(self):
 
        return self.question_text
 
    def check_answer(self, choice):
        return self.choice_set.filter(id=choice.id, is_answer=True).exists()
 
    def get_answers(self):
        return self.choice_set.filter(is_answer=True)
# Create your models here.