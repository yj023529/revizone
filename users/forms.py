from django import forms
from django.contrib.auth.models import User

from django.contrib.auth.forms import UserCreationForm


class UserRegistrationForm(UserCreationForm):
	email = forms.EmailField() ##add required = false to make email not required
	school = forms.CharField()

	class Meta:
		model = User
		fields = ['username','first_name', 'last_name', 'email', 'school', 'password1', 'password2']
	

	