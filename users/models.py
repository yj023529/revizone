from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    school = models.CharField( max_length = 50 )
    quiz_preference = models.BooleanField(default=False)
    quizprogress = models.IntegerField( default = 0)
    notesprogress = models.IntegerField( default = 0)
    molecules_progress = models.IntegerField( default = 0)
    cells_progress = models.IntegerField( default = 0)
    organ_exchange_progress = models.IntegerField( default = 0)
    genes_info_progress = models.IntegerField( default = 0)
    energy_transfer = models.IntegerField( default = 0)
    organ_response_progress = models.IntegerField( default = 0)
    genes_pop_progress = models.IntegerField( default = 0)
    gene_control_progress = models.IntegerField( default = 0)


    def __str__(self):
        return f'{self.user.username} Profile'



# Create your models here.
